@extends('layouts.admin')

@section('breadcrumbs')
<ol class="breadcrumb">
  <li class="active">Dashboard</li>
</ol>
@stop

@section('content')
<div class="col-sm-12">
  <div class="widget">
    <div class="header">
      <div>
        <i class="fa fa-table"></i> <!-- Title --> Think Sumo Dashboard
      </div>
    </div>
    <hr/>
    <div class="dash-quote">
      <div class="col-lg-8 col-md-10">
          <h2 class="text-center">{!! $quote !!}</h2>
          <h4 class="text-right">{{ $quote_by }}</h3>
          </h4>
      </div>
    </div>
  </div>
</div>
@stop
